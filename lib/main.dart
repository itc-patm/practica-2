import 'package:flutter/material.dart';
import 'package:practica_2/src/assets/configuration.dart';
import 'package:practica_2/src/screen/dashboard.dart';
import 'package:practica_2/src/screen/favorites.dart';
import 'package:practica_2/src/screen/login.dart';
import 'package:practica_2/src/screen/search.dart';
import 'package:practica_2/src/screen/splashscreen.dart';
import 'package:practica_2/src/screen/trending.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Configuration conf = Configuration();
  bool activeSession = await conf.getActiveSession();

  runApp((activeSession != null)? (activeSession)? MyAppI() : MyApp() : MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Login(),
      routes: {
        '/login' : (BuildContext context) => Login(),
        '/dashboard' : (BuildContext context) => Dashboard(),
        '/trending' : (BuildContext context) => Trending(),
        '/search' : (BuildContext context) => Search(),
        '/favorites' : (BuildContext context) => Favorites()
      },
    );
  }
}

class MyAppI extends StatefulWidget {
  MyAppI({Key key}) : super(key: key);

  @override
  _MyAppIState createState() => _MyAppIState();
}

class _MyAppIState extends State<MyAppI> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Splashscreen(),
      routes: {
        '/login' : (BuildContext context) => Login(),
        '/dashboard' : (BuildContext context) => Dashboard(),
        '/trending' : (BuildContext context) => Trending(),
        '/search' : (BuildContext context) => Search(),
        '/favorites' : (BuildContext context) => Favorites()
      },
    );
  }
}