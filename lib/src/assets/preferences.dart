import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  SharedPreferences prefs;
  Preferences(SharedPreferences prefs);

  Future <bool> setActiveSession(bool status) async {
    return await prefs.setBool('activeSession', status);
  }

  Future <bool> getActiveSession() async {
    return prefs.getBool('activeSession');
  }

  Future <bool> setToken(String status) async {
    return await prefs.setString('Token', status);
  }

  Future <String> getToken() async {
    return prefs.getString('Token');
  }

  Future <bool> setEmail(String status) async {
    return await prefs.setString('Email', status);
  }

  Future <String> getEmail() async {
    return prefs.getString('Email');
  }

  Future <bool> removeActiveSession() async {
    return await prefs.remove('activeSession');
  }

  Future < String > getString(String key) async {
    String value = prefs.getString(key) ?? null;
    return value;
  }

  Future < bool > delete() async {
    return prefs.clear();
  }

}