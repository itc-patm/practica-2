import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Configuration {
  static Color colorItem = Colors.green;
  static Color colorApp = Colors.orangeAccent;

  Future <bool> setActiveSession(bool status) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setBool('activeSession', status);
  }

  Future <bool> getActiveSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('activeSession');
  }

  Future <bool> setToken(String status) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString('Token', status);
  }

  Future <String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('Token');
  }

  Future <bool> setEmail(String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString('Email', email);
  }

  Future <String> getEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('Email');
  }

  Future <bool> setID(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt('ID', id);
  }

  Future <int> getID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('ID');
  }

  Future <bool> removeActiveSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.remove('activeSession');
  }

  Future < String > getString(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = prefs.getString(key) ?? null;
    return value;
  }

  Future < bool > delete() async {
    final SharedPreferences pre = await SharedPreferences.getInstance();
    return pre.clear();
  }


}