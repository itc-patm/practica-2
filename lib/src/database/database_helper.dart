import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:practica_2/src/models/trending.dart';
import 'package:practica_2/src/models/userdao.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _nameDB = 'PATM2020';
  static final _versionDB = 1;

  static Database _database;

  Future<Database> get getDatabase async {
    if(_database != null) return _database;
    
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory folder = await getApplicationDocumentsDirectory();
    String routeDB = join(folder.path, _nameDB);
    return await openDatabase(
      routeDB, 
      version: _versionDB,
      onCreate: _createTables
    );
  }

  _createTables(Database db, int version) async {
    await db.execute("CREATE TABLE tbl_perfil( id INTEGER PRIMARY KEY, username VARCHAR(30), password VARCHAR(30), nameUser VARCHAR(30), lastPUser VARCHAR(30), lastMUser VARCHAR(30), phone CHAR(12), email VARCHAR(50), photo VARCHAR(255) )");
    await db.execute("CREATE TABLE tbl_favorites( idFav INTEGER PRIMARY KEY, popularity decimal(52), vote_count integer(11), video boolean, poster_path varchar(100), id integer(11), adult boolean, backdrop_path varchar(100), original_title varchar(100), title varchar(100), vote_average decimal(52), overview varchar(100), release_date varchar(100) )");
  }

  Future<int> insert(Map<String, dynamic> row, String table) async {
    var dbClient = await getDatabase;
    return await dbClient.insert(table, row);
  }

  Future<int> update(Map<String, dynamic> row, String table) async {
    var dbClient = await getDatabase;
    return await dbClient.update(table, row, where: 'id = ?', whereArgs: [row['id']]);
  }

  Future<int> delete(int id, String table) async {
    var dbClient = await getDatabase;
    return await dbClient.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  Future<UserDAO> getUser(String email) async {
    var dbClient = await getDatabase;
    var result = await dbClient.query('tbl_perfil', where: 'email = ?', whereArgs: [email]);
    var list = (result).map((element) => UserDAO.fromJSON(element)).toList();
    return list.length > 0 ? list[0] : null;
  }

  Future<List<Result>> getFavorites() async {
    var dbClient = await getDatabase;
    var result = await dbClient.query('tbl_favorites' );
    var lista = (result).map((item) => Result.fromJSON(item)).toList();
    print(lista);
    return lista;
  }

  Future<bool> isFavorite(int id) async {
    var dbClient = await getDatabase;
    var result = await dbClient.query('tbl_favorites', where: 'id = ?', whereArgs: [id] );
    var lista = (result).map((item) => UserDAO.fromJSON(item)).toList();
    print(lista.length > 0 ? "Si esta en favoritos" : "No lo está");
    return lista.length > 0 ? true : false ;
  }

}