import 'package:flutter/material.dart';
import 'package:practica_2/src/database/database_helper.dart';
import 'package:practica_2/src/models/credit.dart';
import 'package:practica_2/src/models/trending.dart';
import 'package:practica_2/src/network/api_movies.dart';
import 'package:practica_2/src/views/card_moviecredits.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class DetailMovie extends StatefulWidget {
  final Result movie;
  final bool isFavorites;
  final String video;

  const DetailMovie(this.movie, this.isFavorites, this.video, {Key key}) : super(key: key);

  @override
  _DetailMovieState createState() => _DetailMovieState();
}

class _DetailMovieState extends State<DetailMovie> {

  DatabaseHelper _database;
  Result movie;
  bool isFavorite;
  String keyVideo;
  Icon iconFav;

  ApiMovies apiDetailMovie;
  @override
  void initState() {
    super.initState();
    _database = DatabaseHelper();
    apiDetailMovie = ApiMovies();

    movie = widget.movie;
    isFavorite = widget.isFavorites;
    keyVideo = widget.video;
    iconFav = new Icon( isFavorite ? Icons.favorite : Icons.favorite_border_outlined );
  }

  @override
  Widget build(BuildContext context) {
    YoutubePlayerController _controller = (keyVideo != null)? YoutubePlayerController(
      initialVideoId: this.keyVideo.toString(),
      flags: YoutubePlayerFlags(
          autoPlay: false,
          mute: false,
          disableDragSeek: false,
          loop: false,
          isLive: false,
          forceHD: false,
      ),
    ): null;

    Container repback = (keyVideo == null)? 
      Container(
        width: MediaQuery.of(context).size.width,
        child: FadeInImage(
          placeholder: AssetImage('assets/01.jpg'),
          image: NetworkImage('https://image.tmdb.org/t/p/w500/${movie.backdropPath}'),
          fadeInDuration: Duration(milliseconds: 100),
        )
      )
    : Container(
        width: MediaQuery.of(context).size.width,
        child: YoutubePlayer(
            controller: _controller,
            liveUIColor: Colors.amber,
        ),
      );


    return Scaffold(
      appBar: AppBar(
          title: Text(movie.title),
          actions: <Widget>[
            IconButton(
              icon: iconFav,
              onPressed: () async {

                if (isFavorite) {
                  await _database.delete( movie.id , "tbl_favorites").then( (data) { 
                    setState(() {
                      iconFav = new Icon(Icons.favorite_border_outlined);
                    });
                    print(data); 
                  });
                } else {
                  await _database.insert(movie.toJSON(), "tbl_favorites").then( (data) { 
                    setState(() {
                      iconFav = new Icon(Icons.favorite);
                    });
                    print(data);
                  });
                }
                
                isFavorite = !isFavorite;
              },
            ),
          ],
        ),
      body: FutureBuilder(
        future: apiDetailMovie.getCredits(movie.id),
        builder: (BuildContext context, AsyncSnapshot<List<Credit>> snapshot) {
          if( snapshot.hasError ) {
            return Center(
              child: Text("Error")
            );
          }else if( snapshot.connectionState == ConnectionState.done) {
            return new Column(
              children: <Widget>[
                repback,
                Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.green[700],
                        ),
                        child: Row(
                          mainAxisAlignment:MainAxisAlignment.center,
                          children: [
                          Text(this.movie.voteAverage.toString(),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              )),
                          SizedBox(width: 10),
                          Icon(Icons.star, color: Colors.white)
                        ])
                      ),
                      ListTile(
                        leading: Icon(Icons.movie),
                        title: Text(this.movie.title),
                        subtitle: Text(this.movie.overview),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: _listDetailMovie(snapshot.data)
                ),
              ],
            );
          }else {
            return Center(
              child: CircularProgressIndicator()
            );
          }
        },
      )
    );
  }

  Widget _listDetailMovie(List<Credit>credits){
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (context,index){
          Credit credit =credits[index];
          return CardMovieCredits(credit : credit);
      },
      itemCount: credits.length
    );
  }
}
