import 'package:flutter/material.dart';
import 'package:practica_2/src/database/database_helper.dart';
import 'package:practica_2/src/models/trending.dart';
import 'package:practica_2/src/views/card_trending.dart';

class Favorites extends StatefulWidget {
  Favorites({Key key}) : super(key: key);

  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {

  DatabaseHelper _database;

  @override
  void initState() {
    super.initState();
    _database = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favorites')
      ),
      body: FutureBuilder(
        future: _database.getFavorites(),
        builder: (BuildContext context, AsyncSnapshot<List<Result>> snapshot) {
          if( snapshot.hasError ) {
            return Center(
              child: Text("Error")
            );
          }else if( snapshot.connectionState == ConnectionState.done) {
            return  _listFavorites(snapshot.data);
          }else {
            return Center(
              child: CircularProgressIndicator()
            );
          }
        },
      )
    );
  }

  Widget _listFavorites(List<Result> movies) {
    return ListView.builder(
      itemBuilder: (context,index) {
        Result favorites=movies[index];
        return CardTrending(trending: favorites);
      },
      itemCount: movies.length
    );
  }

}