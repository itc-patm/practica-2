class Video {

  String id;
  String iso_639_1;
  String iso_3166_1;
  String key;
  String name;
  String site;
  String type;
  int size;

  Video({
    this.id,
    this.iso_639_1,
    this.iso_3166_1,
    this.key,
    this.name,
    this.site,
    this.type,
    this.size,
  });
  
  
  factory Video.fromJSON(Map<String,dynamic> map){
    return Video(
      id       : map['id'],
      iso_639_1  : map['iso_639_1'],
      iso_3166_1 : map['iso_3166_1'],
      key : map['key'],
      name  : map['name'],
      site : map['site'],
      type     : map['type'],
      size     : map['size'],
    );
  }

  Map<String,dynamic> toJSON(){
    return {
      "id"       : id,
      "iso_639_1"  : iso_639_1,
      "iso_3166_1" : iso_3166_1,
      "key" : key,
      "name"  : name,
      "site" : site,
      "type"     : type,
      "size"     : size,
    };
  }
}
