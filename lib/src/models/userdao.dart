import 'dart:convert';

class UserDAO {
  int id;
  String nameUser, lastPUser, lastMUser, phone, email, photo, username, password, accessToken;

  UserDAO({this.id, this.username, this.password, this.nameUser, this.lastPUser, this.lastMUser, this.phone, this.email, this.photo, this.accessToken});

  factory UserDAO.fromJSON(Map<String, dynamic> map) {
    return UserDAO(
      id        : map['id'],
      username  : map['username'],
      password  : map['password'],
      nameUser  : map['nameUser'],
      lastPUser : map['lastPUser'],
      lastMUser : map['lastMUser'],
      phone     : map['phone'],
      email     : map['email'],
      photo     : map['photo'],
      accessToken  : map['accessToken']
    );
  }

  Map<String, dynamic> toJSON() {
    return {
      "id"          : id,
      "username"    : username,
      "password"    : password,
      "nameUser"    : nameUser,
      "lastPUser"   : lastPUser,
      "lastMUser"   : lastMUser,
      "phone"       : phone,
      "email"       : email,
      "photo"       : photo
    };
  }

  String userToJSON() {
    final mapUser = this.toJSON();
    return json.encode(mapUser);
  }

}