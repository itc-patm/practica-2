import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:practica_2/src/models/credit.dart';
import 'package:practica_2/src/models/trending.dart';
import 'package:practica_2/src/models/video.dart';

class ApiMovies {
  final String ENDPOINT = 'https://api.themoviedb.org/3/movie/';
  Client http = Client();

  Future<List<Result>> getTrending() async {
    final response = await http.get(ENDPOINT+'popular?api_key=56937e86789a1dbb468fa42c88926b9d&language=en-US&page=1');
    
    if (response.statusCode == 200){
      var movies = jsonDecode(response.body)['results'] as List;
      return movies.map((movie) => Result.fromJSON(movie)).toList();
    } else {
      return null;
    }
  }

  Future<String> getVideo (int idMovie) async  {
    final response = await http.get(ENDPOINT+"${idMovie}/videos?api_key=56937e86789a1dbb468fa42c88926b9d&language=en-US");

    if (response.statusCode == 200){
      var movies = jsonDecode(response.body)['results'] as List;
      List<Video> listVideos = movies.map((movie) => Video.fromJSON(movie)).toList();
      return  listVideos.length == 0 ? null : listVideos[0].key ;
    } else {
      return null;
    }
  }

  Future<List<Credit>> getCredits (int idMovie) async  {
    final response = await http.get(ENDPOINT+"${idMovie}/credits?api_key=56937e86789a1dbb468fa42c88926b9d&language=es-LA");
    
    if (response.statusCode == 200){
      var movies = jsonDecode(response.body)['cast'] as List;
      return  movies.map((movie) => Credit.fromJSON(movie)).toList();
    } else {
      return null;
    }
  }


}